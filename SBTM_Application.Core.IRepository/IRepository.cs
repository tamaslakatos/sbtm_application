﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SBTM_Application.Core.IRepository
{
    public interface IRepository<T>
        where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
        T Add(T entity);
        T Delete(T entity);
        void Edit(T entity);
        void Save();
    }
}