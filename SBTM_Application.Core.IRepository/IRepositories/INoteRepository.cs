﻿using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Core.IRepository
{
    public interface INoteRepository : IRepository<Note>
    {
        Note GetById(int id);
    }
}