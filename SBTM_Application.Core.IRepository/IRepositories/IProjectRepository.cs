﻿using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Core.IRepository
{
   public interface IProjectRepository : IRepository<Project>
    {
        Project GetById(int id);
    }
}