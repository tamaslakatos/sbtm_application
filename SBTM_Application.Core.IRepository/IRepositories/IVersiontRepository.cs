﻿using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Core.IRepository
{
    public interface IVersionRepository : IRepository<Version>
    {
        Version GetById(int id);
    }
}
