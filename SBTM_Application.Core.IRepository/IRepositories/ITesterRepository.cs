﻿using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Core.IRepository
{
    public interface ITesterRepository : IRepository<Tester>
    {
        Tester GetById(int id);
    }
}