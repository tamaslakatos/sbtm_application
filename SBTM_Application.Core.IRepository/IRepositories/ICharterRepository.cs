﻿using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Core.IRepository
{
    public interface ICharterRepository : IRepository<Charter>
    {
        Charter GetById(int id);
    }
}