﻿using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Core.IService
{
    public interface INoteService : IService<Note>
    {
        Note GetById(int Id);
    }
}