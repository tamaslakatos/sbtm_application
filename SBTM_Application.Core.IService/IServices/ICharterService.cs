﻿using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Core.IService
{
    public interface ICharterService : IService<Charter>
    {
        Charter GetById(int Id);
    }
}