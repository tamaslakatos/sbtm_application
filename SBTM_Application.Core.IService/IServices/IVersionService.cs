﻿using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Core.IService
{
    public interface IVersionService : IService<Version>
    {
        Version GetById(int Id);
    }
}