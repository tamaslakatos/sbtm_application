﻿namespace SBTM_Application.Core.IService
{
    public interface IJiraApiService
    {
        string GetJiraAPIResource(
           string resource,
           string argument = null,
           string data = null,
           string method = "GET");
    }
}
