﻿using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Core.IService
{
    public interface IProjectService : IService<Project>
    {
        Project GetById(int Id);
    }
}