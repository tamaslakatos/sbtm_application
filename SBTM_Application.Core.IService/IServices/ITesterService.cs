﻿using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Core.IService
{
    public interface ITesterService : IService<Tester>
    {
        Tester GetById(int Id);
    }
}