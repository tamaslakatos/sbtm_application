﻿using System;

namespace SBTM_Application.Core.IService
{
    public interface IUnitOfWork : IDisposable
    {
        int Commit();
    }
}