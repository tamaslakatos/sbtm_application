﻿using System.Collections.Generic;

namespace SBTM_Application.Core.IService
{
    public interface IService<T>
        where T : class
    {
        void Create(T entity);
        void Delete(T entity);
        IEnumerable<T> GetAll();
        void Update(T entity);
    }
}