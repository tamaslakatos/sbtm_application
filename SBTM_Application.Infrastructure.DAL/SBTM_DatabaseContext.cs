﻿using System.Data.Entity;
using SBTM_Application.Infrastructure.DAL.Mapping;

namespace SBTM_Application.Infrastructure.DAL
{
    public partial class SBTM_DatabaseContext : DbContext
    {
        public SBTM_DatabaseContext()
            : base("Name=SBTM_Database")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TesterMap());
            modelBuilder.Configurations.Add(new ProjectMap());
            modelBuilder.Configurations.Add(new VersionMap());
            modelBuilder.Configurations.Add(new CharterMap());
            modelBuilder.Configurations.Add(new NoteMap());
        }
    }
}