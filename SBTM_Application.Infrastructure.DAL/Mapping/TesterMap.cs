﻿using System.Data.Entity.ModelConfiguration;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Infrastructure.DAL.Mapping
{
    public class TesterMap : EntityTypeConfiguration<Tester>
    {
        public TesterMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.UserName)
                .IsRequired()
                .HasMaxLength(30);

            Property(t => t.FullName)
              .IsRequired()
              .HasMaxLength(30);

            Property(t => t.Email)
              .IsRequired()
              .HasMaxLength(30);

            Property(t => t.Password)
              .IsRequired()
              .HasMaxLength(30);

            Property(t => t.Company)
              .IsRequired()
              .HasMaxLength(30);

            // Table & Column Mappings
            ToTable("Testers");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.UserName).HasColumnName("UserName");
            Property(t => t.FullName).HasColumnName("FullName");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.Password).HasColumnName("Password");
            Property(t => t.Company).HasColumnName("Company");

            // Relationships
            HasMany(p => p.Projects)
                .WithMany(t => t.Testers)
                .Map(pt =>
                    {
                        pt.MapLeftKey("Id");
                        pt.MapRightKey("JiraId");
                        pt.ToTable("Tester_Project");
                    });
        }
    }
}
