﻿using System.Data.Entity.ModelConfiguration;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Infrastructure.DAL.Mapping
{
    public class CharterMap : EntityTypeConfiguration<Charter>
    {
        public CharterMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Area)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.CharterText)
                .IsRequired()
                .HasMaxLength(300);

            Property(t => t.Risk)
                .HasMaxLength(30)
                .IsRequired();

            Property(t => t.TesterName)
                .IsRequired()
                .HasMaxLength(30);

            Property(t => t.Progress)
                .HasMaxLength(30)
                .IsRequired();

            // Table & Column Mappings
            ToTable("Charters");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.VersionId).HasColumnName("VersionId");
            Property(t => t.Area).HasColumnName("Area");
            Property(t => t.CharterText).HasColumnName("CharterText");
            Property(t => t.Risk).HasColumnName("Risk");
            Property(t => t.TesterName).HasColumnName("TesterName");
            Property(t => t.Progress).HasColumnName("Progress");

            // Relationships
            HasMany(c => c.Notes)
                .WithRequired(v => v.Charter)
                .HasForeignKey(fk => fk.CharterId);
        }
    }
}