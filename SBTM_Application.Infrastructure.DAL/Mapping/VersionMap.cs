﻿using System.Data.Entity.ModelConfiguration;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Infrastructure.DAL.Mapping
{
    public class VersionMap : EntityTypeConfiguration<Version>
    {
        public VersionMap()
        {
            // Primary Key
            HasKey(t => t.JiraId);

            // Properties
            Property(t => t.VersionName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("Versions");
            Property(t => t.JiraId).HasColumnName("JiraId");
            Property(t => t.ProjectId).HasColumnName("ProjectId");
            Property(t => t.VersionName).HasColumnName("VersionName");

            // Relationships
            HasMany(c => c.Charters)
                .WithRequired(v => v.Version)
                .HasForeignKey(fk => fk.VersionId);
        }
    }
}