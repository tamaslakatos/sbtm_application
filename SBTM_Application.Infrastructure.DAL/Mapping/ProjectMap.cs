﻿using System.Data.Entity.ModelConfiguration;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Infrastructure.DAL.Mapping
{
    public class ProjectMap : EntityTypeConfiguration<Project>
    {
        public ProjectMap()
        {
            //Primary Key
            HasKey(t => t.JiraId);

            // Properties
            Property(t => t.ProjectName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("Projects");
            Property(t => t.JiraId).HasColumnName("JiraId");
            Property(t => t.ProjectName).HasColumnName("ProjectName");

            // Relationships
            HasMany(v => v.Versions)
                .WithRequired(p => p.Project)
                .HasForeignKey(fk => fk.ProjectId);
        }
    }
}