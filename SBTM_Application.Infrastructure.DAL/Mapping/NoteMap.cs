﻿using System.Data.Entity.ModelConfiguration;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Infrastructure.DAL.Mapping
{
    public class NoteMap : EntityTypeConfiguration<Note>
    {
        public NoteMap()
        {
            //Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Type)
                .IsRequired();

            Property(t => t.Description)
                .HasMaxLength(300)
                .IsRequired();

            Property(t => t.TimeSpent)
                .IsRequired();

            // Table & Column Mappings
            ToTable("Notes");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.CharterId).HasColumnName("CharterId");
            Property(t => t.Type).HasColumnName("Type");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.TimeSpent).HasColumnName("TimeSpent");
        }
    }
}