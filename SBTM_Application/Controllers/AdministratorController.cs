﻿using System.Linq;
using System.Web.Mvc;
using SBTM_Application.Core.Domain.Model;
using SBTM_Application.Core.IService;

namespace SBTM_Application.Controllers
{
    public class AdministratorController : Controller
    {
        private ITesterService TesterService;

        public AdministratorController(ITesterService testerService)
        {
            TesterService = testerService;
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        [HttpPost]
        public void AddTester(Tester addTester)
        {
            TesterService.Create(addTester);
        }

        [HttpGet]
        public JsonResult GetAllTesters()
        {
            var allTesters = TesterService.GetAll()
                    .Select(t => new
                    {
                        t.UserName,
                        t.FullName,
                        t.Email,
                        t.Password,
                        t.Company
                    })
                    .ToArray();
            return Json(allTesters, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void UpdateTester(Tester updatedTester)
        {
            var testerToUpdate = TesterService.GetById(updatedTester.Id);
            testerToUpdate.UserName = updatedTester.UserName;
            testerToUpdate.FullName = updatedTester.FullName;
            testerToUpdate.Email = updatedTester.Email;
            testerToUpdate.Password = updatedTester.Password;
            testerToUpdate.Company = updatedTester.Company;
            TesterService.Update(testerToUpdate);
        }

        [HttpPost]
        public void DeleteTester(Tester deleteTester)
        {
            TesterService.Delete(deleteTester);
        }
    }
}