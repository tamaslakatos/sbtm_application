﻿using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Mvc;
using SBTM_Application.Core.Domain.Model;
using SBTM_Application.Core.IService;
using System.Data;

namespace SBTM_Application.Controllers
{
    public class TesterController : Controller
    {
        #region Service Interfaces

        private IProjectService ProjectService;
        private IVersionService VersionService;
        private ICharterService CharterService;
        private INoteService NoteService;
        private IJiraApiService JiraApiService;

        #endregion 

        public TesterController(IProjectService projectService, IVersionService versionService,
                ICharterService charterService, INoteService noteService, IJiraApiService jiraApiService)
        {
            ProjectService = projectService;
            VersionService = versionService;
            CharterService = charterService;
            NoteService = noteService;
            JiraApiService = jiraApiService;
        }

        public ActionResult Home()
        {        
            return View();
        }

        [HttpGet]
        public JsonResult GetAllProjects()
        {
            var allProjects = ProjectService.GetAll()
                    .Select(p => new { p.JiraId, p.ProjectName })
                    .ToArray();          
            return Json(allProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllVersions(int id)
        {
            var allVersions = VersionService.GetAll()
                    .Where(p => p.ProjectId == id)
                    .Select(v => new { v.JiraId, v.VersionName })
                    .ToArray();
            return Json(allVersions, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllCharters(int id)
        {
            var allCharters = CharterService.GetAll()
                    .Where(v => v.VersionId == id)
                    .Select(c => new
                    {
                        c.Id,
                        c.Area,
                        c.CharterText,
                        c.Risk,
                        c.TesterName,
                        c.Progress,
                    })
                    .ToArray();
            return Json(allCharters, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllNotes(int id)
        {
            var allNotes = NoteService.GetAll()
                    .Where(c => c.CharterId == id)
                    .Select(n => new
                    {
                        n.Id,
                        n.Type,
                        n.Description,
                        n.TimeSpent
                    })
                    .ToArray();
            return Json(allNotes, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetJiraProjects()
        {
            string projectsString = JiraApiService.GetJiraAPIResource("project");
            var jiraProjects = JsonConvert.DeserializeObject<List<Project>>(projectsString);
            return Json(jiraProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetJiraVersions(int id)
        {
            string versionsString = JiraApiService.GetJiraAPIResource("project/" + id.ToString() + "/versions");
            var jiraVersions = JsonConvert.DeserializeObject<List<Core.Domain.Model.Version>>(versionsString);
            return Json(jiraVersions, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void AddJiraProject(Project addProject)
        {
            ProjectService.Create(addProject);
        }

        [HttpPost]
        public void AddJiraVersion(Core.Domain.Model.Version addVersion)
        {
            VersionService.Create(addVersion);
        }

        [HttpPost]
        public void AddCharter(Charter addCharter)
        {
            CharterService.Create(addCharter);
        }

        [HttpPost]
        public void UpdateSessionProgress(Charter updateCharter)
        {
            var charterToUpdate = CharterService.GetById(updateCharter.Id);
            charterToUpdate.Progress = updateCharter.Progress;
            CharterService.Update(charterToUpdate);
        }

        [HttpPost]
        public void SaveSessionNotes(IEnumerable<Note> addNotes)
        {
            foreach (Note note in addNotes)
            {
                NoteService.Create(note);
            }
        }
    }
}