﻿app.controller('TesterHomeController', function ($rootScope, $http, $scope, $compile, ProjectService, VersionService, CharterService, SessionService) {

    // List of possible states that the progress of a charter can have.

    var charterProgressStates = {
        NotStarted: "Not Started",
        InProgress: "In Progress",
        Finished: "Finished"
    }

    // Retrieves the projects from database.

    ProjectService.getJiraProjects().then(function (projects) {
        if (!SessionService.getIsSessionRunning()) {
            $scope.projects = projects;
        }
        else {
            $rootScope.message = "Session currently in progress."
            var element = angular.element(document.querySelector('.wrapper'));
            var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
            element.after(appendHtml);
        }
    });

    // Retrieves the versions from database.

    $scope.getVersions = function (selectedProject) {
        if (!SessionService.getIsSessionRunning()) {
            $scope.currentlySelectedProject = selectedProject;
            VersionService.getJiraVersions(selectedProject.JiraId).then(function (versions) {
                $scope.versions = versions;
            });
        }
        else {
            $rootScope.message = "Session currently in progress."
            var element = angular.element(document.querySelector('.wrapper'));
            var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
            element.after(appendHtml);
        }
    };

    // Retrieves the charters from database.

    $scope.getCharters = function (selectedVersion) {
        if (!SessionService.getIsSessionRunning()) {
            $scope.currentlySelectedVersion = selectedVersion;
            CharterService.getCharters(selectedVersion.JiraId).then(function (charters) {
                $scope.charters = charters;
            });
        }
        else {
            $rootScope.message = "Session currently in progress."
            var element = angular.element(document.querySelector('.wrapper'));
            var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
            element.after(appendHtml);
        }
    };

    // Pushes the data to the list when new data is added from the different services.

    $scope.$on('handleAddProject', function (events, projectToAdd) {
        $scope.projects.push(projectToAdd);
    });

    $scope.$on('handleAddVersion', function (events, versionToAdd) {
        $scope.versions.push(versionToAdd);
    });

    $scope.$on('handleAddCharter', function (events, charterToAdd) {
        $scope.charters.push(charterToAdd);
    });

    // Appends the pop-up box to select jira resource.

    $scope.getJiraProject = function () {
        if (!SessionService.getIsSessionRunning()) {
            var element = angular.element(document.querySelector('.wrapper'));
            var appendHtml = $compile('<project-box></project-box>')($scope);
            element.after(appendHtml);
        }
        else {
            $rootScope.message = "Session currently in progress."
            var element = angular.element(document.querySelector('.wrapper'));
            var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
            element.after(appendHtml);
        }
    }

    $scope.getJiraVersion = function () {
        if (!SessionService.getIsSessionRunning()) {
            if ($('#left-selected li').hasClass('active')) {
                $rootScope.projectId = $scope.currentlySelectedProject.JiraId;
                var element = angular.element(document.querySelector('.wrapper'));
                var appendHtml = $compile('<version-box></version-box>')($scope);
                element.after(appendHtml);
            }
            else {
                $rootScope.message = "Select a project."
                var element = angular.element(document.querySelector('.wrapper'));
                var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
                element.after(appendHtml);
            };
        }
        else {
            $rootScope.message = "Session currently in progress."
            var element = angular.element(document.querySelector('.wrapper'));
            var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
            element.after(appendHtml);
        }
    };

    // Toggles the side navigation bar when clicked.  

    $("#toggle-icon").click(function (e) {
        e.preventDefault();
        $(".wrapper").toggleClass("toggled");
    });

    // Toggle session notes table when clicked on arrow.

    $('#charter-table-body').on('click', 'td.arrow-up', function () {
        $rootScope.CharterId = $scope.currentlySelectedCharter.Id;
        $(this).parent().addClass('drop-down');
        var element = angular.element(document.querySelector('#charter-table-body tr.drop-down'));
        var appendHtml = $compile('<notes-table></notes-table>')($scope);
        element.after(appendHtml);
        $('#charter-table-body tr.drop-down td.arrow-up').removeClass('arrow-up');
        $('#charter-table-body tr.drop-down').removeClass('drop-down');
        $(this).addClass('arrow-down');
    });

    $('#charter-table-body').on('click', 'td.arrow-down', function () {
        $(this).removeClass('arrow-down');
        $(this).addClass('arrow-up');
        $rootScope.$broadcast('handleCloseNotes');
    });

    // Side navigation bar select effect.

    $('#left-selected').on('click', 'li', function () {
        $('#left-selected li').removeClass('active');
        $(this).addClass('active');
        $('#right-selected li').removeClass('active');
    });

    $('#right-selected').on('click', 'li', function () {
        $('#right-selected li').removeClass('active');
        $(this).addClass('active');
        $('tr.charter-row').removeClass('active');
    });

    // Charter list select effect. 

    $('#charter-data-table tbody').on('click', 'tr.charter-table-row', function () {
        $('tr.charter-table-row').removeClass('active');
        $(this).addClass('active');
    });

    // Charter button functionality.

    $scope.addCharter = function () {
        if (!SessionService.getIsSessionRunning()) {
            if ($('#right-selected li').hasClass('active')) {
                $rootScope.versionId = $scope.currentlySelectedVersion.JiraId;
                $rootScope.Progress = charterProgressStates.NotStarted;
                var element = angular.element(document.querySelector('.wrapper'));
                var appendHtml = $compile('<charter-box></charter-box>')($scope);
                element.after(appendHtml);
            }
            else {
                $rootScope.message = "Select a version."
                var element = angular.element(document.querySelector('.wrapper'));
                var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
                element.after(appendHtml);
            };
        }
        else {
            $rootScope.message = "Session currently in progress."
            var element = angular.element(document.querySelector('.wrapper'));
            var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
            element.after(appendHtml);
        }
    };

    $scope.editCharter = function (selectedCharter) {
        if (!SessionService.getIsSessionRunning()) {
            if ($('#charter-table-body tr.charter-table-row').hasClass('active')) {
            }
            else {
                $rootScope.message = "Select a charter."
                var element = angular.element(document.querySelector('.wrapper'));
                var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
                element.after(appendHtml);
            }
        }
        else {
            $rootScope.message = "Session currently in progress."
            var element = angular.element(document.querySelector('.wrapper'));
            var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
            element.after(appendHtml);
        }
    }

    $scope.startSession = function (selectedCharter) {
        if (!SessionService.getIsSessionRunning()) {
            if ($('#charter-table-body tr.charter-table-row').hasClass('active')) {
                if (selectedCharter.Progress == charterProgressStates.Finished || selectedCharter.Progress == charterProgressStates.InProgress) {
                    $rootScope.message = "Session finished or in progress.";
                    var element = angular.element(document.querySelector('.wrapper'));
                    var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
                    element.after(appendHtml);
                }
                else {
                    $rootScope.charter = selectedCharter;
                    CharterService.changeState(selectedCharter, charterProgressStates.InProgress);
                    SessionService.setIsSessionRunning(true);
                    var element = angular.element(document.querySelector('#charter-table-body tr.active'));
                    var appendHtml = $compile('<session-table></session-table>')($scope);
                    element.after(appendHtml);
                }
            }
            else {
                $rootScope.message = "Select a charter.";
                var element = angular.element(document.querySelector('.wrapper'));
                var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
                element.after(appendHtml);
            }
        }
        else {
            $rootScope.message = "Session currently in progress."
            var element = angular.element(document.querySelector('.wrapper'));
            var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
            element.after(appendHtml);
        }
    }

    $scope.endSession = function () {
        if (SessionService.getIsSessionRunning()) {
            $rootScope.Progress = charterProgressStates.Finished;
            var element = angular.element(document.querySelector('.wrapper'));
            var appendHtml = $compile('<end-session-box></end-session-box>')($scope);
            element.after(appendHtml);
        }
        else {
            $rootScope.message = "No session currently in progress.";
            var element = angular.element(document.querySelector('.wrapper'));
            var appendHtml = $compile('<reminder-box></reminder-box>')($scope);
            element.after(appendHtml);
        }
    }

    // Sets the currently selected charter from list.

    $scope.selectCharter = function (charter) {
        $scope.currentlySelectedCharter = charter;
    }
});