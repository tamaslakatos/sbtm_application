﻿app.controller("AdministratorDashboardController", function ($rootScope, $scope, $compile, TesterService) {

    // Retrieves the testers from database.

    TesterService.getTesters().then(function (testers) {
        $scope.testers = testers;
    });

    $scope.addTester = function () {
            var element = angular.element(document.querySelector('.alma'));
            var appendHtml = $compile('<tester-box></tester-box>')($scope);
            element.after(appendHtml);
    }

    // Pushes the data to the tester list when new tester is added from the Testerservices.

    $scope.$on('handleAddTester', function (events, testerToAdd) {
        $scope.testers.push(testerToAdd);
    });

    // Sets the currently selected tester from list.

    $scope.selectTester = function (tester) {
        $scope.currentlySelectedTester = tester;
    }

    // Tester list select effect. 

    $('#tester-data-table tbody').on('click', 'tr.tester-table-row', function () {
        $('tr.tester-table-row').removeClass('active');
        $(this).addClass('active');
    });

    $scope.deleteTester = function (selectedTester) {
        TesterService.deleteTester(selectedTester);
    };

    $scope.editTester = function (selectedTester) {
       
    };
});