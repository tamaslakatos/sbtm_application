﻿app.factory("CharterService", function ($http, $rootScope) {

    var charters = [];

    return {
        getCharters: function (versionId) {
            return $http.get('/Tester/GetAllCharters/' + versionId).then(function (response) {
                charters = response.data;
                return charters;
            });
        },

        saveCharter: function (charterToAdd) {
            return $http.post('/Tester/AddCharter/', charterToAdd).success(function () {
                $rootScope.$broadcast('handleAddCharter', charterToAdd);
            });
        },

        changeState: function (charter, state) {
            charter.Progress = state;
            return $http.post('/Tester/UpdateSessionProgress/', charter).success(function () {
            });
        },
    }
});