﻿app.factory("SessionService", function ($http) {

    var isSessionRunning = false;
    var notes = [];

    return {
        getSessionNotes: function (charterId) {
            return $http.get('/Tester/GetAllNotes/' + charterId).then(function (response) {
                notes = response.data;
                return notes;
            });
        },

        saveSessionNotes: function () {
            return $http.post('/Tester/SaveSessionNotes/', notes).success(function () {
                notes = [];
            });
        },

        addSessionNote: function (noteToAdd) {
            notes.push(noteToAdd);
        },

        getIsSessionRunning: function () {
            return isSessionRunning;
        },

        setIsSessionRunning: function (value) {
            isSessionRunning = value;
        },
    }
});