﻿app.factory("ProjectService", function ($http, $rootScope) {

    var jiraProjects = [];

    return {
        getJiraProjects: function () {
            return $http.get('/Tester/GetAllProjects/').then(function (response) {
                jiraProjects = response.data;
                return jiraProjects;
            });
        },

        getJiraProjectsAPI: function () {
            return $http.get('/Tester/GetJiraProjects/').then(function (response) {
                var jiraProjectsAPI = $.map(response.data, function (dataItem) { return { value: dataItem.ProjectName, data: dataItem.JiraId }; })
                return jiraProjectsAPI;
            })
        },

        saveJiraProject: function (projectToAdd) {
            return $http.post('/Tester/AddJiraProject/', projectToAdd).success(function () {
                $rootScope.$broadcast('handleAddProject', projectToAdd);
            });
        }
    }
});