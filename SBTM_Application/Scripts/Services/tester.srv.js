﻿app.factory("TesterService", function ($http, $rootScope) {

    var testers = [];

    return {
        getTesters: function () {
            return $http.get('/Administrator/GetAllTesters/').then(function (response) {
                testers = response.data;
                return testers;
            });
        },

        saveTester: function (testerToAdd) {
            return $http.post('/Administrator/AddTester/', testerToAdd).success(function () {
                $rootScope.$broadcast('handleAddTester', testerToAdd);
            });
        },

        deleteTester: function (testerToDelete) {
            return $http.post('/Administrator/DeleteTester/', testerToDelete).success(function () {
                $rootScope.$broadcast('handleDeleteTester', testerToDelete);
            });
        }
    }
});