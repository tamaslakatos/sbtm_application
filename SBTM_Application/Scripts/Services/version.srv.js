﻿app.factory("VersionService", function ($http, $rootScope) {

    var jiraVersions = [];

    return {
        getJiraVersions: function (projectId) {
            return $http.get('/Tester/GetAllVersions/' + projectId).then(function (response) {
                jiraVersions = response.data;
                return jiraVersions;
            });
        },

        getJiraVersionsAPI: function (projectId) {
            return $http.get('/Tester/GetJiraVersions/' + projectId).then(function (response) {
                var jiraVersionsAPI = $.map(response.data, function (dataItem) { return { value: dataItem.VersionName, data: dataItem.JiraId }; })
                return jiraVersionsAPI;
            })
        },

        saveJiraVersion: function (versionToAdd) {
            return $http.post('/Tester/AddJiraVersion/', versionToAdd).success(function () {
                $rootScope.$broadcast('handleAddVersion', versionToAdd);
            });
        }
    }
});