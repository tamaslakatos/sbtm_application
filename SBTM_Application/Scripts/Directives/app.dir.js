﻿app.directive('testerBox', function (TesterService) {
    return {
        replace: true,
        scope: {},
        templateUrl: '/Scripts/Templates/tester-box.html',
        controller: function ($scope, $element) {

            $scope.saveTester = function () {

                var testerToAdd = {
                    UserName: $scope.userName,
                    FullName: $scope.fullName,
                    Email: $scope.email,
                    Password: $scope.password,
                    Company: $scope.company,
                };

                TesterService.saveTester(testerToAdd);
            }

            $scope.CloseBox = function (event) {
                $element.remove();
                $scope.$destroy();
            }
        }
    };
});

app.directive('projectBox', function (ProjectService) {
    return {
        restrict: "E",
        replace: true,
        scope: {},
        templateUrl: '/Scripts/Templates/project-box.html',
        controller: function ($rootScope, $scope, $element) {

            // Retrieves projects from Jira API.

            ProjectService.getJiraProjectsAPI().then(function (jiraProjects) {
                $('#autocomplete-projects').autocomplete({
                    lookup: jiraProjects,
                    onSelect: function (suggestion) {
                        $scope.suggestedProject = { ProjectName: suggestion.value, JiraId: suggestion.data };
                    },
                });
            });

            $scope.saveJiraProject = function (projectToAdd) {
                if ($('#add-project').valid()) {
                    alert('Valid');
                    ProjectService.saveJiraProject(projectToAdd);
                } else {
                    alert('Not Valid');
                }
            };

            $scope.CloseBox = function (event) {
                $element.remove();
                $scope.$destroy();
            }
        }
    };
});

app.directive('versionBox', function (VersionService) {
    return {
        restrict: "E",
        replace: true,
        scope: {},
        templateUrl: '/Scripts/Templates/version-box.html',
        controller: function ($rootScope, $scope, $element) {

            var selectedProjectId = $rootScope.projectId

            // Retrieves versions from Jira API.

            VersionService.getJiraVersionsAPI(selectedProjectId).then(function (jiraVersions) {
                $('#autocomplete-versions').autocomplete({
                    lookup: jiraVersions,
                    onSelect: function (suggestion) {
                        $scope.suggestedVersion = { VersionName: suggestion.value, JiraId: suggestion.data, ProjectId: selectedProjectId };
                    }
                });
            });

            $scope.saveVersion = function (versionToAdd) {
                VersionService.saveJiraVersion(versionToAdd);
            }

            $scope.CloseBox = function (event) {
                $element.remove();
                $scope.$destroy();
            }
        }
    };
});

app.directive('charterBox', function (CharterService) {
    return {
        replace: true,
        scope: {},
        templateUrl: '/Scripts/Templates/charter-box.html',
        controller: function ($rootScope, $scope, $element) {

            $scope.saveCharter = function () {

                var charterToAdd = {
                    VersionId: $rootScope.versionId,
                    Area: $scope.area,
                    CharterText: $scope.charterText,
                    Risk: $scope.risk,
                    TesterName: "Tamas",
                    Progress: $rootScope.Progress
                };

                CharterService.saveCharter(charterToAdd);
            }

            $scope.CloseBox = function (event) {
                $element.remove();
                $scope.$destroy();
            }
        }
    };
});

app.directive('reminderBox', function () {
    return {
        replace: true,
        scope: {},
        templateUrl: '/Scripts/Templates/reminder-box.html',
        controller: function ($rootScope, $scope, $element) {

            $scope.message = $rootScope.message;

            $scope.CloseBox = function (event) {
                $element.remove();
                $scope.$destroy();
            }
        }
    };
});

app.directive('sessionTable', function (SessionService) {
    return {
        scope: true,
        replace: true,
        scope: {},
        templateUrl: '/Scripts/Templates/session-table.html',
        controller: function ($rootScope, $scope, $element) {

            $scope.notes = [];
            $scope.hideNoteSelector = true;
            var isFirstSelection = true;

            $scope.selectSessionNote = function () {
                $scope.hideNoteSelector = false;
            }

            $scope.addNoteToSession = function (obj) {

                note = {
                    CharterId: $rootScope.charter.Id,
                    Type: obj.target.attributes.data.value,
                    Description: "",
                    TimeSpent: "",
                };

                $scope.notes.push(note);
                SessionService.addSessionNote(note);

                $scope.hideNoteSelector = $scope.hideNoteSelector === false ? true : false;

                if (isFirstSelection) {
                    isFirstSelection = false;
                }
                else {
                    var b = $scope.notes.length - 2;
                    $rootScope.n = b;
                    document.getElementsByTagName('timer')[b].stop();
                    $scope.notes[$scope.notes.length - 2].TimeSpent = $rootScope.getTimeSpent;
                }
            };

            $scope.$on('endSession', function (events) {
                $element.remove();
                $scope.$destroy();
            });
        }
    };
});

app.directive('endSessionBox', function (SessionService, CharterService) {
    return {
        scope: true,
        replace: true,
        scope: {},
        templateUrl: '/Scripts/Templates/end-session-box.html',
        controller: function ($rootScope, $scope, $element) {

            $scope.saveSession = function () {
                document.getElementsByTagName('timer')[$rootScope.n + 1].stop();

                CharterService.changeState($rootScope.charter, $rootScope.Progress);
                SessionService.saveSessionNotes();
                SessionService.setIsSessionRunning(false);
                $rootScope.$broadcast('endSession');
            }

            $scope.CloseBox = function (e) {
                $element.remove();
                $scope.$destroy();
            }
        }
    };
});

app.directive('notesTable', function (SessionService) {
    return {
        replace: true,
        scope: true,
        templateUrl: '/Scripts/Templates/notes-table.html',
        controller: function ($rootScope, $scope, $element) {

            SessionService.getSessionNotes($rootScope.CharterId).then(function (notes) {
                $scope.sessionNotes = notes;
            });

            $scope.$on('handleCloseNotes', function (events) {
                $element.remove();
                $scope.$destroy();
            });
        }
    };
});