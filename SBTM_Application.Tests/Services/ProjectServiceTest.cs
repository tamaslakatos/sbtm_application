﻿using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SBTM_Application.Core.Domain.Model;
using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.IService;
using SBTM_Application.Service;
using System.Collections.Generic;
using System.Linq;

namespace SBTM_Application.Tests
{
    [TestClass]
    public class ProjectServiceTest
    {
        private Mock<IProjectRepository> MockProjectRepository;
        private Mock<IUnitOfWork> MockUnitOfWork;
        private IEnumerable<Project> Projects;
        private IProjectService ProjectService;

        [TestInitialize]
        public void InitializeTest()
        {
            Projects = new List<Project>()
            {
                new Project { JiraId = 15514, ProjectName = "Forecaster Support SD" },
                new Project { JiraId = 15516, ProjectName = "Marketing Solutions SD" },
                new Project { JiraId = 15517, ProjectName = "IT SUPPORT SD" },
                new Project { JiraId = 16900, ProjectName = "Forecaster Insight SD" },
                new Project { JiraId = 17002, ProjectName = "Lavitta SD" }
            };

            MockProjectRepository = new Mock<IProjectRepository>();
            MockUnitOfWork = new Mock<IUnitOfWork>();
            ProjectService = new ProjectService(MockUnitOfWork.Object, MockProjectRepository.Object);
        }

        [TestMethod]
        public void TestGetAllProjectsMethod()
        {
            int expectedProjects = 4;
            MockProjectRepository.Setup(p => p.GetAll()).Returns(Projects);
            var allProjects = ProjectService.GetAll().ToArray();
            Assert.IsNotNull(allProjects);
            Assert.AreEqual(expectedProjects, allProjects.Count());
        }

        [TestMethod]
        public void TestAddProjectMethod()
        {
            int jiraId = 17200;
            Project projectToAdd = new Project() { ProjectName = "FINLUX" };

            MockProjectRepository.Setup(p => p.Add(projectToAdd)).Returns((Project p) =>
            {
                p.JiraId = jiraId;
                return p;
            });

            ProjectService.Create(projectToAdd);
            Assert.AreEqual(jiraId, projectToAdd.JiraId);
            MockUnitOfWork.Verify(w => w.Commit(), Times.Once);
        }
    }
}
