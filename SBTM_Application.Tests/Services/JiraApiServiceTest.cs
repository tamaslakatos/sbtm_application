﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SBTM_Application.Core.Domain.Model;
using SBTM_Application.Core.IService;
using SBTM_Application.Service;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SBTM_Application.Tests.Services
{
    [TestClass]
    public class JiraApiServiceTest
    {
        private IJiraApiService JiraApiService;

        [TestInitialize]
        public void InitializeTest()
        {
            JiraApiService = new JiraApiService();
        }

        [TestMethod]
        public void GetJiraProjectResource()
        {
            string projectsString = JiraApiService.GetJiraAPIResource("project");
            var jiraProjects = JsonConvert.DeserializeObject<List<Project>>(projectsString);
            Assert.IsNotNull(jiraProjects);
        }

        [TestMethod]
        public void GetJiraVersionResource()
        {
            int projectId = 15514;
            string versionsString = JiraApiService.GetJiraAPIResource("project/" + projectId.ToString() + "/versions");
            var jiraVersions = JsonConvert.DeserializeObject<List<Version>>(versionsString);
            Assert.IsNotNull(jiraVersions);
            foreach (Version version in jiraVersions)
            {
                Assert.AreEqual(projectId, version.ProjectId);
            }
        }
    }
}
