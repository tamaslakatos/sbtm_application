﻿using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SBTM_Application.Core.Domain.Model;
using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.IService;
using SBTM_Application.Service;
using System.Collections.Generic;
using System.Linq;

namespace SBTM_Application.Tests
{
    [TestClass]
    public class TesterServiceTest
    {
        private Mock<ITesterRepository> MockTesterRepository;
        private Mock<IUnitOfWork> MockUnitOfWork;
        private IEnumerable<Tester> Testers;
        private ITesterService TesterService;

        [TestInitialize]
        public void InitializeTest()
        {
            Testers = new List<Tester>()
            {
                new Tester { Id = 1, UserName = "tamaslakatos", FullName = "Tamas Lakatos", Email = "tamas.lakatos@outlook.com", Company = "Lillebaelt", Password = "AE8:H#)U"},
                new Tester { Id = 2, UserName = "robertjohnson", FullName = "Robert Johnson", Email = "rob.jo@yahoo.com", Company = "FLIX", Password = "Dh^&65%$!@"},
                new Tester { Id = 3, UserName = "stevenwilson", FullName = "Steven Wilson", Email = "st.will@gmail.com", Company = "KOTI", Password = "QW_7e2%l!+"}
            };

            MockTesterRepository = new Mock<ITesterRepository>();
            MockUnitOfWork = new Mock<IUnitOfWork>();
            TesterService = new TesterService(MockUnitOfWork.Object, MockTesterRepository.Object);
        }

        [TestMethod]
        public void TestGetAllTestersMethod()
        {
            int expectedTesters = 3;
            MockTesterRepository.Setup(t => t.GetAll()).Returns(Testers);
            var allTesters = TesterService.GetAll().ToArray();
            Assert.IsNotNull(allTesters);
            Assert.AreEqual(expectedTesters, allTesters.Count());
        }

        [TestMethod]
        public void TestAddTesterMethod()
        {
            int id = 4;
            Tester testerToAdd = new Tester() { Id = 4, UserName = "jackconoly", FullName = "Jack Conoly", Email = "jc.co@gmail.com", Company = "WORD", Password = "^&HFjg76fik" };
            MockTesterRepository.Setup(p => p.Add(testerToAdd)).Returns((Tester t) =>
            {
                t.Id = id;
                return t;
            });

            TesterService.Create(testerToAdd);
            Assert.AreEqual(id, testerToAdd.Id);
            MockUnitOfWork.Verify(w => w.Commit(), Times.Once);
        }
    }
}
