﻿using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SBTM_Application.Core.Domain.Model;
using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.IService;
using SBTM_Application.Service;
using System.Collections.Generic;
using System.Linq;

namespace SBTM_Application.Tests
{
    [TestClass]
    public class VersionServiceTest
    {
        private Mock<IVersionRepository> MockVersionRepository;
        private Mock<IUnitOfWork> MockUnitOfWork;
        private IEnumerable<Version> Versions;
        private IVersionService VersionService;

        [TestInitialize]
        public void InitializeTest()
        {
            Versions = new List<Version>()
            {
                new Version { ProjectId = 15514, JiraId = 32600, VersionName = "Forecaster 2014.7.1 Release" },
                new Version { ProjectId = 15514, JiraId = 37601, VersionName = "Forecaster 2014.12.2" },
                new Version { ProjectId = 15514, JiraId = 39800, VersionName = "Forecaster release 5" },
                new Version { ProjectId = 15514, JiraId = 40404, VersionName = "Forecaster release 8" },
            };

            MockVersionRepository = new Mock<IVersionRepository>();
            MockUnitOfWork = new Mock<IUnitOfWork>();
            VersionService = new VersionService(MockUnitOfWork.Object, MockVersionRepository.Object);
        }

        [TestMethod]
        public void TestGetAllVersionsMethod()
        {
            int expectedVersions = 4;
            MockVersionRepository.Setup(v => v.GetAll()).Returns(Versions);
            var allVersions = VersionService.GetAll().ToArray();
            Assert.IsNotNull(allVersions);
            Assert.AreEqual(expectedVersions, allVersions.Count());
        }

        [TestMethod]
        public void TestAddVersionMethod()
        {
            int jiraId = 40608;
            int projectId = 17300;

            Version versionToAdd = new Version() { VersionName = "Phase 1 - study phase" };

            MockVersionRepository.Setup(p => p.Add(versionToAdd)).Returns((Version v) =>
            {
                v.JiraId = jiraId;
                v.ProjectId = projectId;
                return v;
            });

            VersionService.Create(versionToAdd);
            Assert.AreEqual(jiraId, versionToAdd.JiraId);
            Assert.AreEqual(projectId, versionToAdd.ProjectId);
            MockUnitOfWork.Verify(w => w.Commit(), Times.Once);
        }
    }
}
