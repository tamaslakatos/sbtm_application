﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Infrastructure.Repository
{
    public class TesterRepository : Repository<Tester>, ITesterRepository
    {
        public TesterRepository(DbContext context)
            : base(context)
        {
        }

        public Tester GetById(int id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public override IEnumerable<Tester> GetAll()
        {
            return Entities.Set<Tester>();
        }
    }
}
