﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Infrastructure.Repository
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public ProjectRepository(DbContext context)
            : base(context)
        {
        }

        public Project GetById(int id)
        {
            return FindBy(p => p.JiraId == id).FirstOrDefault();
        }

        public override IEnumerable<Project> GetAll()
        {
            return Entities.Set<Project>();
        }
    }
}
