﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Infrastructure.Repository
{
    public class VersionRepository : Repository<Version>, IVersionRepository
    {
        public VersionRepository(DbContext context)
            : base(context)
        {
        }

        public Version GetById(int id)
        {
            return FindBy(x => x.JiraId == id).FirstOrDefault();
        }

        public override IEnumerable<Version> GetAll()
        {
            return Entities.Set<Version>();
        }
    }
}
