﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Infrastructure.Repository
{
    public class NoteRepository : Repository<Note>, INoteRepository
    {
        public NoteRepository(DbContext context)
            : base(context)
        {
        }

        public Note GetById(int id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public override IEnumerable<Note> GetAll()
        {
            return Entities.Set<Note>();
        }
    }
}
