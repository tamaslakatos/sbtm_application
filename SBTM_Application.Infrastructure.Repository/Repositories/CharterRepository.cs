﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Infrastructure.Repository
{
    public class CharterRepository : Repository<Charter>, ICharterRepository
    {
        public CharterRepository(DbContext context)
            : base(context)
        {
        }

        public Charter GetById(int id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public override IEnumerable<Charter> GetAll()
        {
            return Entities.Set<Charter>();
        }
    }
}