﻿using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.IService;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Service
{
    public class TesterService : Service<Tester>, ITesterService
    {
        private IUnitOfWork UnitOfWork;
        private ITesterRepository TesterRepository;

        public TesterService(IUnitOfWork unitOfWork, ITesterRepository testerRepository)
            : base(unitOfWork, testerRepository)
        {
            UnitOfWork = unitOfWork;
            TesterRepository = testerRepository;
        }

        public Tester GetById(int Id)
        {
            return TesterRepository.GetById(Id);
        }
    }
}
