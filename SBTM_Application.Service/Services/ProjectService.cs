﻿using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.IService;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Service
{
    public class ProjectService : Service<Project>, IProjectService
    {
        private IUnitOfWork UnitOfWork;
        private IProjectRepository ProjectRepository;

        public ProjectService(IUnitOfWork unitOfWork, IProjectRepository projectRepository)
            : base(unitOfWork, projectRepository)
        {
            UnitOfWork = unitOfWork;
            ProjectRepository = projectRepository;
        }

        public Project GetById(int Id)
        {
            return ProjectRepository.GetById(Id);
        }
    }
}