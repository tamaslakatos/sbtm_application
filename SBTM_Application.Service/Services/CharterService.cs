﻿using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.IService;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Service
{
    public class CharterService : Service<Charter>, ICharterService
    {
        private IUnitOfWork UnitOfWork;
        private ICharterRepository CharterRepository;

        public CharterService(IUnitOfWork unitOfWork, ICharterRepository charterRepository)
            : base(unitOfWork, charterRepository)
        {
            UnitOfWork = unitOfWork;
            CharterRepository = charterRepository;
        }

        public Charter GetById(int Id)
        {
            return CharterRepository.GetById(Id);
        }
    }
}