﻿using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.IService;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Service
{
    public class NoteService : Service<Note>, INoteService
    {
        private IUnitOfWork UnitOfWork;
        private INoteRepository NotesRepository;

        public NoteService(IUnitOfWork unitOfWork, INoteRepository notesRepository)
            : base(unitOfWork, notesRepository)
        {
            UnitOfWork = unitOfWork;
            NotesRepository = notesRepository;
        }

        public Note GetById(int Id)
        {
            return NotesRepository.GetById(Id);
        }
    }
}