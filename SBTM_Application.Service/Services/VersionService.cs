﻿using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.IService;
using SBTM_Application.Core.Domain.Model;

namespace SBTM_Application.Service
{
    public class VersionService : Service<Version>, IVersionService
    {
        private IUnitOfWork UnitOfWork;
        private IVersionRepository VersionRepository;

        public VersionService(IUnitOfWork unitOfWork, IVersionRepository versionRepository)
            : base(unitOfWork, versionRepository)
        {
            UnitOfWork = unitOfWork;
            VersionRepository = versionRepository;
        }

        public Version GetById(int Id)
        {
            return VersionRepository.GetById(Id);
        }
    }
}
