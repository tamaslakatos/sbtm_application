﻿using System.Collections.Generic;
using SBTM_Application.Core.IRepository;
using SBTM_Application.Core.IService;

namespace SBTM_Application.Service
{
    public abstract class Service<T> : IService<T>
        where T : class
    {
        protected IUnitOfWork UnitOfWork;
        protected IRepository<T> Repository;

        public Service(IUnitOfWork unitOfWork, IRepository<T> repository)
        {
            UnitOfWork = unitOfWork;
            Repository = repository;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return Repository.GetAll();
        }

        public virtual void Create(T entity)
        {
            Repository.Add(entity);
            UnitOfWork.Commit();
        }

        public virtual void Update(T entity)
        {
            Repository.Edit(entity);
            UnitOfWork.Commit();
        }

        public virtual void Delete(T entity)
        {
            Repository.Delete(entity);
            UnitOfWork.Commit();
        }
    }
}