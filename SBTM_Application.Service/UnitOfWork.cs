﻿using System;
using SBTM_Application.Core.IService;
using System.Data.Entity;

namespace SBTM_Application.Service
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private DbContext DbContext;

        public UnitOfWork(DbContext context)
        {
            DbContext = context;
        }

        public int Commit()
        {
            return DbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                    DbContext = null;
                }
            }
        }
    }
}