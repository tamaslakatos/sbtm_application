﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBTM_Application.Core.Domain.Model
{
    public class Note
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int CharterId { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public TimeSpan TimeSpent { get; set; }
        public Charter Charter { get; set; }
    }
}
