﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBTM_Application.Core.Domain.Model
{
    public class Charter
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int VersionId { get; set; }
        public string Area { get; set; }
        public string CharterText { get; set; }
        public string Risk { get; set; }
        public string TesterName { get; set; }
        public string Progress { get; set; }
        public virtual Version Version { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
    }
}