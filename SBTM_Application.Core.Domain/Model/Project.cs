﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBTM_Application.Core.Domain.Model
{
    public partial class Project
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [JsonProperty("id")]
        public int JiraId { get; set; }

        [JsonProperty("name")]
        public string ProjectName { get; set; }

        public virtual ICollection<Tester> Testers { get; set; }
        public virtual ICollection<Version> Versions { get; set; }
    }
}