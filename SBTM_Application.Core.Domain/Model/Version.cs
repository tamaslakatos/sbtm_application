﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBTM_Application.Core.Domain.Model
{
    public partial class Version
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [JsonProperty("id")]
        public int JiraId { get; set; }

        [JsonProperty("name")]
        public string VersionName { get; set; }

        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }
        public virtual ICollection<Charter> Charters { get; set; }
    }
}