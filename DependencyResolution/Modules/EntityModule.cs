﻿using Autofac;
using SBTM_Application.Core.IService;
using SBTM_Application.Service;
using SBTM_Application.Infrastructure.DAL;
using System.Data.Entity;

namespace DependencyResolution.Modules
{
    public class EntityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new RepositoryModule());
            builder.RegisterType(typeof(SBTM_DatabaseContext)).As(typeof(DbContext)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(UnitOfWork)).As(typeof(IUnitOfWork)).InstancePerRequest();
            builder.RegisterType(typeof(JiraApiService)).As(typeof(IJiraApiService)).InstancePerRequest();
        }
    }
}